import React from 'react'

const EditItem = props => {
  if(props.popup)
  {
    return (
      <div class="popup">
        <form>
          <div className="form-group">
            <input type="text" className="form-control" name="name" value={props.name} onChange={ props.handleInputChange}/>
          </div>
          <div className="form-group">
            <input type="text" className="form-control" name="email" value={props.email} onChange={ props.handleInputChange} />
          </div>
          <div className="form-group">
            <input type="text" className="form-control" name="phone" value={props.phone} onChange={ props.handleInputChange} />
          </div>
          <button onClick={ props.updateItem } className="btn btn-success mt-2"> Update </button>
          <button onClick={() => props.setEditing(false)} className="btn btn-info mt-2">Cancel</button>
        </form>
      </div>
    )
  }
  else{
    return null;
  }
}

export default EditItem;