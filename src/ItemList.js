import React from 'react';

  const ItemList = (props) => {
    return (  
      <table className="table table-bordered">
        <thead>
          <tr>
            <td colSpan={5}>
              <button onClick={() => props.popupShow(true, true)}>Add Record</button>
            </td>
          </tr>
          <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
            { 
              props.Items.length > 0 ? (
                props.Items.map((Item) => (
                    <tr key={Item.id}>
                      <td>{ Item.id }</td>
                      <td>{ Item.name }</td>
                      <td>{ Item.email }</td>
                      <td>{ Item.phone }</td>
                      <td>
                        <button className="btn btn-primary ml-2" onClick={() => props.editItem(Item) }>Edit</button>
                        <button className="btn btn-danger ml-2" onClick={() => props.deleteItem(Item.id) }>Delete</button>                       

                      </td>
                    </tr>
                  )
                )
              ) : (
                <tr>
                  <td colSpan={5}>No records</td>
                </tr>                
              )
            }
        </tbody>
      </table>

    );
  }


export default ItemList;


