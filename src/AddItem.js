import React from 'react'

const AddItem = props => {
  if(props.popup)
  {
    return (
      <div class="popup">
        <form onSubmit={ props.addItem }>
          <div className="form-group">
            <input type="text" className="form-control" name="name" value={props.name} onChange={ props.handleInputChange} placeholder="Name"/>
          </div>
          <div className="form-group">
            <input type="text" className="form-control" name="email" value={props.email} onChange={ props.handleInputChange} placeholder="Email"/>
          </div>
          <div className="form-group">
            <input type="text" className="form-control" name="phone" value={props.phone} onChange={ props.handleInputChange} placeholder="Phone"/>
          </div>
          <button className="btn btn-success mt-2"> Add </button>
          <button className="btn btn-danger mt-2" onClick={() => props.popupShow(false)}> Cancel </button>
        </form>
      </div>
    )
  }
  else{
    return null;
  }
  
}

export default AddItem;