import React from 'react';
import ItemList from './ItemList';
import AddItem from './AddItem';
import './App.css';
import EditItem from './EditItem';
class App extends React.Component {

  constructor() {
    super();
    let arrItems = JSON.parse(localStorage.getItem('tempData') || JSON.stringify([]));

    this.state = {
      id: null,
      userId: 1,
      name: '',
      email: '',
      phone: '',      
      Item: {},
      Items: arrItems,
      editing: false,
      showPopup:false,
    };
    
    this.handleInputChange = this.handleInputChange.bind(this);
    this.deleteItem = this.deleteItem.bind(this);
    this.addItem = this.addItem.bind(this);
    this.editItem = this.editItem.bind(this);
    this.setEditing = this.setEditing.bind(this);
    this.updateItem = this.updateItem.bind(this);
    this.popupShow = this.popupShow.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
 
    this.setState({
      [name]:value
    })
  }

  addItem(event){
    event.preventDefault()
    if (!this.state.name) return;
    const Item = {
      id: this.state.Items.length + 1,
      name: this.state.name,
      email: this.state.email,
      phone: this.state.phone,
      userId: this.state.userId,
    };
    this.setState({
      name: '',
      email: '',
      phone: '',
      Item: Item,
      showPopup:false,
      Items: [...this.state.Items, Item]
    })
    localStorage.setItem('tempData', JSON.stringify([...this.state.Items, Item]))
    console.log(this.state.Items);
  }


  setEditing(value) {
    this.setState({
      editing: value
    })
    if(!value)
    {
      this.setState({
        showPopup: false
      })
    }
  }
  popupShow(value, clear) {
    if(clear)
    {
      this.setState({
        name:"",
        email:'',
        phone:'',
        showPopup: value
      })
    }
    else{
      this.setState({
        showPopup: value
      })
    }
    
  }

  deleteItem(id) {
    const Items = this.state.Items.filter( item => item.id !== id );
    this.setState({Items: Items});
    localStorage.setItem('tempData', JSON.stringify(Items));
    if(this.state.editing === true) {
      window.location.reload();
    }
  }

  editItem(Item) {
    this.setEditing(true);
    this.setState({
      name:Item.name,
      email:Item.email,
      phone:Item.phone,
      Item: Item,
      showPopup:true,
    });
  }

 
  updateItem(event) {
    event.preventDefault();
    const updatedName = this.state.name;
    const updatedEmail = this.state.email;
    const updatedPhone = this.state.phone;
    const updatedItem = Object.assign({}, this.state.Item, { name: updatedName, email: updatedEmail, phone: updatedPhone })
    const Items = this.state.Items.map((Item) => (Item.id === this.state.Item.id ? updatedItem : Item));

    this.setState({ name:'', phone: '', email: '', Items: Items, showPopup:false});

    localStorage.setItem('tempData', JSON.stringify(Items));

  }

 

  render() {
    const {Items, editing } = this.state;
      return (
        <div className="App">
          <div className="row App-main">
            <ItemList 
              Items= {Items} 
              deleteItem={this.deleteItem}
              editItem={this.editItem}
              popupShow={this.popupShow}
            />
          </div>
          <div className="row App-main">
          { 
            editing  ? (
            <EditItem 
             name={this.state.name}
             email={this.state.email} 
             phone={this.state.phone} 
             handleInputChange={this.handleInputChange}
             setEditing={this.setEditing}
             updateItem={this.updateItem}
             popup={this.state.showPopup}
             popupShow={this.popupShow}
            />
            ) : (
            <AddItem 
              name={this.state.name}
              email={this.state.email} 
              phone={this.state.phone} 
              popup={this.state.showPopup}
              popupShow={this.popupShow}
              handleInputChange={this.handleInputChange} 
              addItem={this.addItem}
            />
            )
          }
          </div>
        </div>
      );
    }
}


export default App;
